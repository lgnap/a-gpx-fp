# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- removed unused setters

## [5.3.0] - 2021-05-02
### Added
- xdebug code coverage

### Fixed
- tests running on gitlab CI


## [5.2.0] - 2021-05-01
### Added
- extract bounds data from gpx

## [5.1.0] - 2021-04-15
### Added
 - Detection of integrated geochecker

## [5.0.0] - 2021-04-06
### Changed
 - Rewriting of all Model object to be more related to gpx file


## [4.0.1] - 2021-04-05
### Changed
 - phpunit as dev deps

## [4.0.0] - 2021-04-05
### Changed
 - Get out docx writer

## [3.2.1] - 2021-04-04
### Fixed
 - Forget to put the changelog into the commit ...

## [3.2.0] - 2021-04-04
### Added
 - A changelog is now in place
### Fixed
 - Error when <cmt> is not present into additionnal wp

## [3.1.2] - 2021-03-22
### Fixed
 - Use geocache name instead of gc code (to sort by on display)

## [3.1.1] - 2021-03-21
### Added
 - Add Gpx files to unit tests

### Fixed
 - support various NS for groundspeak xml

## [3.1.0] - 2021-03-21
### Added
 - Unit tests
 - Enable strict types
 - extract longitude & latitude

### Fixed
 - some issues popped through unit tests

## [3.0.0] - 2021-03-20
### Changed
 - Docx generator works now with string[][] and not GpxWaypoint[]

## [2.3.1] - 2021-03-20
### Fixed
 - Forgot getAdditionals call

## [2.3.0] - 2021-03-19
### Changed
 - Try to map every additionnal waypoint in geocache's attributes

## [2.2.0] - 2021-03-17
### Added
 - Support waypoints in addition to geocaches

### Removed
 - Only one template is now integrated to tests

### Fixed
 - Sometimes GSAK is not used to generate gpx, so gsak wp could be null

## [2.1.2] - 2021-03-15
### Fixed
 - Sometimes GSAK is not used to generate gpx, so gsak wp could be null

## [2.1.1] - 2021-03-15
### Fixed
 - Difficulty & Terrain in cache are floats

### Other
 - Little mistake in semver here. We should release 2.0.1 and not 2.1.1 

## [2.0.0] - 2021-03-14
### Changed
 - Is it possible to get a tmp file where docx is generated

## [1.1.0] - 2021-03-13
### Changed
 - Use GCCode as key for result array

### Fixed
 - Retrieve only cache that have a gs:cache node

## [1.0.1] - 2021-03-13
### Added
 - Add a license (GPL)

## [1.0.0] - 2021-03-13
### Added
 - First initial public release
