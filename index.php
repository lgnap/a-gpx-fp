<?php

use LGnap\Reader\GpxReader;
use LGnap\Writer\DocxWriter;

require_once 'vendor/autoload.php';

$gpxFiles = glob(__DIR__  . '/tests/gpx-files/*.gpx' );

foreach ($gpxFiles as $gpxFile) {
    $gpxReader = new GpxReader($gpxFile);

    $gpxWaypoints = $gpxReader->extractWaypoints();
//    var_dump($gpxWaypoints);
}
