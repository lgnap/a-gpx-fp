<?php

namespace LGnap\Reader;

use DOMNode;
use DOMXPath;
use LGnap\Model\Gpx\GpxGsak;

class GsakWaypointReader extends DOMXPathReader
{
    public function __construct(DOMXPath $DOMXPath)
    {
        parent::__construct($DOMXPath);
        $this->DOMXPath->registerNamespace('gsak', 'http://www.gsak.net/xmlv1/6');
    }

    public function extractWaypoint(DOMNode $waypoint): ?GpxGsak
    {
        $gcCodeNode = $this->DOMXPath->query('./gsak:wptExtension/gsak:GcNote', $waypoint);
        if (!$gcCodeNode || $gcCodeNode->length !== 1) {
            return null;
        }

        return new GpxGsak($gcCodeNode->item(0)->nodeValue);
    }
}
