<?php
declare(strict_types=1);

namespace LGnap\Reader;

use DOMNode;
use LGnap\Model\Gpx\GpxWpt;

class WaypointReader extends DOMXPathReader
{
    public function extractWaypoint(DOMNode $waypoint): GpxWpt
    {
        $name = (string) $waypoint->getElementsByTagName('name')->item(0)->nodeValue;
        $longitude = (float) $waypoint->attributes->getNamedItem('lon')->nodeValue;
        $latitude = (float) $waypoint->attributes->getNamedItem('lat')->nodeValue;
        $cmtNode = $waypoint->getElementsByTagName('cmt');
        $comment = $cmtNode->count() ? $cmtNode->item(0)->nodeValue: '';
        $description = $waypoint->getElementsByTagName('desc')->item(0)->nodeValue;
        $type = $waypoint->getElementsByTagName('type')->item(0)->nodeValue;

        return new GpxWpt($name, $latitude, $longitude, $description, $comment, $type);
    }
}
