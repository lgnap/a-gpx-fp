<?php
declare(strict_types=1);

namespace LGnap\Reader;

use DOMDocument;
use DOMXPath;
use LGnap\Model\Bound;
use LGnap\Model\Gpx\GpxWptNode;
use LGnap\Model\GpxWpt;

class GpxReader
{

    private DOMXPath $domXPath;

    public function __construct(string $filename)
    {
        $domDoc = new DOMDocument();
        $domDoc->load($filename);

        $this->domXPath = new DOMXPath($domDoc);
        $this->domXPath->registerNamespace('g', 'http://www.topografix.com/GPX/1/0');

    }

    public function extractBound(): ?Bound
    {
        $domNodes = $this->domXPath->query('/g:gpx/g:bounds');

        if (! $domNodes->count()) {
            return null;
        }

        $item = $domNodes->item(0);

        return new Bound(
            (float) $item->attributes->getNamedItem('minlat')->nodeValue,
            (float) $item->attributes->getNamedItem('minlon')->nodeValue,
            (float) $item->attributes->getNamedItem('maxlat')->nodeValue,
            (float) $item->attributes->getNamedItem('maxlon')->nodeValue
        );
    }

    /**
     * @return GpxWptNode[]
     */
    public function extractWaypoints(): array
    {
        $gpxWaypoints = [];

        $waypointReader = new WaypointReader($this->domXPath);
        $waypointGsak = new GsakWaypointReader($this->domXPath);
        $waypointGsReader = new GroundspeakReader($this->domXPath);

        foreach ($this->domXPath->query('/g:gpx/g:wpt') as $waypoint) {
            $wpt = $waypointReader->extractWayPoint($waypoint);
            $groundspeak = $waypointGsReader->extractWaypoint($waypoint);
            $gsak = $waypointGsak->extractWaypoint($waypoint);

            $gpxWaypoints[$wpt->getName()] = new GpxWptNode($wpt, $groundspeak, $gsak);
        }

        return $gpxWaypoints;
    }
}
