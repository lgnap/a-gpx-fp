<?php


namespace LGnap\Reader;

use DOMNode;
use DOMXPath;

abstract class DOMXPathReader
{
    protected DOMXPath $DOMXPath;

    public abstract function extractWaypoint(DOMNode $waypoint);

    public function __construct(DOMXPath $DOMXPath)
    {
        $this->DOMXPath = $DOMXPath;
    }
}
