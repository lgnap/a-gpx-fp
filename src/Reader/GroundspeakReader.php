<?php


namespace LGnap\Reader;

use DOMDocument;
use DOMNode;
use DOMXPath;
use LGnap\Model\Gpx\GpxGroundspeak;

class GroundspeakReader extends DOMXPathReader
{
    public function __construct(DOMXPath $DOMXPath)
    {
        parent::__construct($DOMXPath);

        $DOMXPath->registerNamespace('groundspeak', $this->retrieveGroundspeakNS());
    }

    public function extractWaypoint(DOMNode $waypoint): ?GpxGroundspeak
    {
        $groundspeakCache = $this->DOMXPath->query('./groundspeak:cache', $waypoint);

        if (!$groundspeakCache->count()) {
            return null;
        }

        $groundspeakCache = $groundspeakCache->item(0);
        $groundspeak =  new GpxGroundspeak();

        foreach ($groundspeakCache->childNodes as $waypointChildNode) {
            switch ($waypointChildNode->nodeName) {
                default: //silent uninteresting nodes
                    break;
                case 'groundspeak:name':
                    $groundspeak->setName($waypointChildNode->nodeValue);
                    break;
                case 'groundspeak:type':
                    $groundspeak->setType($waypointChildNode->nodeValue);
                    break;
                case 'groundspeak:difficulty':
                    $groundspeak->setDifficulty((float) $waypointChildNode->nodeValue);
                    break;
                case 'groundspeak:terrain':
                    $groundspeak->setTerrain((float) $waypointChildNode->nodeValue);
                    break;
                case 'groundspeak:encoded_hints':
                    $groundspeak->setHint($waypointChildNode->nodeValue);
                    break;
                case 'groundspeak:long_description':
                    $groundspeak->setLongDescription($waypointChildNode->nodeValue);
                    break;
                case 'groundspeak:short_description':
                    $groundspeak->setShortDescription($waypointChildNode->nodeValue);
                    break;
            }
        }

        $geocheckerNode = $this->DOMXPath->query('./groundspeak:attributes/groundspeak:attribute[@id=72]', $groundspeakCache);

        $groundspeak->setHasChecker($geocheckerNode->count());

        return $groundspeak;
    }


    /**
     * @return string|null
     */
    private function retrieveGroundspeakNS(): ?string
    {
        $elements = $this->DOMXPath->document->getElementsByTagNameNS('*', 'cache');
        return ($elements->count()) ? $elements->item(0)->namespaceURI : null;
    }

}
