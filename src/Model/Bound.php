<?php

namespace LGnap\Model;

class Bound
{
    private float $minLatitude;
    private float $minLongitude;

    private float $maxLatitude;
    private float $maxLongitude;

    /**
     * Bound constructor.
     * @param float $minLatitude
     * @param float $minLongitude
     * @param float $maxLatitude
     * @param float $maxLongitude
     */
    public function __construct(float $minLatitude, float $minLongitude, float $maxLatitude, float $maxLongitude)
    {
        $this->minLatitude = $minLatitude;
        $this->minLongitude = $minLongitude;
        $this->maxLatitude = $maxLatitude;
        $this->maxLongitude = $maxLongitude;
    }

    /**
     * @return float
     */
    public function getMinLatitude(): float
    {
        return $this->minLatitude;
    }

    /**
     * @return float
     */
    public function getMaxLatitude(): float
    {
        return $this->maxLatitude;
    }

    /**
     * @return float
     */
    public function getMinLongitude(): float
    {
        return $this->minLongitude;
    }

    /**
     * @return float
     */
    public function getMaxLongitude(): float
    {
        return $this->maxLongitude;
    }
}