<?php
declare(strict_types=1);

namespace LGnap\Model\Gpx;

class GpxWpt
{
    private string $name;
    private float $latitude;
    private float $longitude;
    private string $description;
    private string $comment;
    private string $type;

    /**
     * Wpt constructor.
     * @param string $name
     * @param float $latitude
     * @param float $longitude
     * @param string $description
     * @param string $comment
     */
    public function __construct(string $name, float $latitude, float $longitude, string $description, string $comment, string $type)
    {
        $this->name = $name;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->description = $description;
        $this->comment = $comment;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}
