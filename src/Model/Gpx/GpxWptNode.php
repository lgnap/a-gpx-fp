<?php
declare(strict_types=1);

namespace LGnap\Model\Gpx;

class GpxWptNode
{
    private GpxWpt $wpt;
    private ?GpxGroundspeak $groundspeak;
    private ?GpxGsak $gsak;

    /**
     * Entry constructor.
     * @param GpxWpt $wpt
     * @param GpxGroundspeak|null $groundspeak
     * @param GpxGsak|null $gsak
     */
    public function __construct(GpxWpt $wpt, ?GpxGroundspeak $groundspeak, ?GpxGsak $gsak)
    {
        $this->wpt = $wpt;
        $this->groundspeak = $groundspeak;
        $this->gsak = $gsak;
    }

    /**
     * @return GpxWpt
     */
    public function getWpt(): GpxWpt
    {
        return $this->wpt;
    }

    /**
     * @return GpxGroundspeak|null
     */
    public function getGroundspeak(): ?GpxGroundspeak
    {
        return $this->groundspeak;
    }

    /**
     * @return GpxGsak|null
     */
    public function getGsak(): ?GpxGsak
    {
        return $this->gsak;
    }
}
