<?php
declare(strict_types=1);

namespace LGnap\Model\Gpx;


class GpxGroundspeak
{
    private string $name;
    private string $type;
    private float $difficulty;
    private float $terrain;
    private string $hint;
    private string $longDescription;
    private string $shortDescription;
    private bool $hasChecker;

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function setDifficulty(float $difficulty)
    {
        $this->difficulty = $difficulty;
    }

    public function setTerrain(float $terrain)
    {
        $this->terrain = $terrain;
    }

    public function setHint(string $hint)
    {
        $this->hint = $hint;
    }

    public function setLongDescription(string $description)
    {
        $this->longDescription = $description;
    }

    public function setShortDescription(string $description)
    {
        $this->shortDescription = $description;
    }

    public function setHasChecker(bool $hasChecker): void
    {
        $this->hasChecker = $hasChecker;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getDifficulty(): float
    {
        return $this->difficulty;
    }

    public function getTerrain(): float
    {
        return $this->terrain;
    }

    public function getHint(): string
    {
        return $this->hint;
    }

    public function getLongDescription(): string
    {
        return $this->longDescription;
    }

    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    public function hasChecker(): bool
    {
        return $this->hasChecker;
    }
}
