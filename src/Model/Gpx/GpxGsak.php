<?php
declare(strict_types=1);

namespace LGnap\Model\Gpx;

class GpxGsak
{
    private string $gcNote;

    public function __construct(string $gcNote)
    {
        $this->gcNote = $gcNote;
    }

    public function getGcNote(): string
    {
        return $this->gcNote;
    }
}
