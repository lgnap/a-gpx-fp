<?php

namespace LGnap\Reader;

use PHPUnit\Framework\TestCase;

class GpxReaderBugsTest extends TestCase
{
    /**
     * @covers \LGnap\Reader\DOMXPathReader
     * @covers \LGnap\Reader\GroundspeakReader
     * @covers \LGnap\Reader\GsakWaypointReader
     * @covers \LGnap\Reader\WaypointReader
     * @covers \LGnap\Reader\GpxReader
     * @covers \LGnap\Model\Gpx\GpxWptNode
     * @covers \LGnap\Model\Gpx\GpxWpt
     * @covers \LGnap\Model\Gpx\GpxGsak
     * @covers \LGnap\Model\Gpx\GpxGroundspeak
     */
    public function testOtherNamespacesForXml()
    {
        $gpxReader = new GpxReader(__DIR__ . '/../gpx-files/MB.gpx');

        $waypoints = $gpxReader->extractWaypoints();

        self::assertIsArray($waypoints);
        self::assertCount(29, $waypoints);
    }

    /**
     * @covers \LGnap\Reader\DOMXPathReader
     * @covers \LGnap\Reader\GroundspeakReader
     * @covers \LGnap\Reader\GsakWaypointReader
     * @covers \LGnap\Reader\WaypointReader
     * @covers \LGnap\Reader\GpxReader
     * @covers \LGnap\Model\Gpx\GpxWptNode
     * @covers \LGnap\Model\Gpx\GpxWpt
     * @covers \LGnap\Model\Gpx\GpxGsak
     * @covers \LGnap\Model\Gpx\GpxGroundspeak
     */
    public function testCmtMissingInParking()
    {
        $gpxReader = new GpxReader(__DIR__ . '/../gpx-files/Bossinet.gpx');
        $waypoints = $gpxReader->extractWaypoints();

        self::assertIsArray($waypoints);
        self::assertCount(1, $waypoints);
    }
}
