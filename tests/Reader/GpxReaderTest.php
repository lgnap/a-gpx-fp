<?php

namespace LGnap\Reader;

use LGnap\Model\Gpx\GpxWptNode;
use LGnap\Model\Gpx\GpxGroundspeak;
use LGnap\Model\Gpx\GpxGsak;
use LGnap\Model\Gpx\GpxWpt;
use PHPUnit\Framework\TestCase;

class GpxReaderTest extends TestCase
{
    /** @covers \LGnap\Reader\GpxReader::__construct */
    public function testConstruct()
    {
        self::assertInstanceOf(GpxReader::class, new GpxReader(__DIR__ . '/../gpx-files/MultiAvecWaypoints.gpx'));
    }

    /**
     * @covers \LGnap\Reader\DOMXPathReader
     * @covers \LGnap\Reader\GroundspeakReader
     * @covers \LGnap\Reader\GsakWaypointReader
     * @covers \LGnap\Reader\WaypointReader
     * @covers \LGnap\Reader\GpxReader
     * @covers \LGnap\Model\Gpx\GpxWptNode
     * @covers \LGnap\Model\Gpx\GpxWpt
     * @covers \LGnap\Model\Gpx\GpxGroundspeak
     */
    public function testExtractWaypointsFromMulti()
    {
        $gpxReader = new GpxReader(__DIR__ . '/../gpx-files/MultiAvecWaypoints.gpx');

        $waypoints = $gpxReader->extractWaypoints();

        self::assertIsArray($waypoints);
        self::assertCount(16, $waypoints);

        $gccodes = array_keys($waypoints);
        array_walk($gccodes, function(string $gccode) {
            self::assertStringEndsWith('5EWZ2', $gccode);
        });
        $waypoint = $waypoints['GC5EWZ2'];
        self::assertInstanceOf(GpxWptNode::class, $waypoint);
        self::assertInstanceOf(GpxWpt::class, $waypoint->getWpt());
        self::assertInstanceOf(GpxGroundspeak::class, $waypoint->getGroundspeak());
        self::assertNull($waypoint->getGsak());

        self::assertSame('GC5EWZ2', $waypoint->getWpt()->getName());
        self::assertSame('[TdW] BW.16 NIVELLES', $waypoint->getGroundspeak()->getName());
        self::assertSame('Multi-cache', $waypoint->getGroundspeak()->getType());
        self::assertSame(1.5, $waypoint->getGroundspeak()->getDifficulty());
        self::assertSame(1.5, $waypoint->getGroundspeak()->getTerrain());
        self::assertSame(4.328367, $waypoint->getWpt()->getLongitude());
        self::assertSame(50.596517, $waypoint->getWpt()->getLatitude());
        self::assertSame('lierre mais facile ;-)', $waypoint->getGroundspeak()->getHint());

        self::assertArrayHasKey('015EWZ2', $waypoints);
        $waypoint = $waypoints['015EWZ2'];
        self::assertInstanceOf(GpxWptNode::class, $waypoint);
        self::assertInstanceOf(GpxWpt::class, $waypoint->getWpt());
        self::assertNull($waypoint->getGroundspeak());
        self::assertNull($waypoint->getGsak());
        self::assertSame('015EWZ2', $waypoint->getWpt()->getName());
        self::assertSame(50.595917, $waypoint->getWpt()->getLatitude());
        self::assertSame(4.327367, $waypoint->getWpt()->getLongitude());
        self::assertSame('Waypoint|Virtual Stage', $waypoint->getWpt()->getType());
        self::assertSame('WP1', $waypoint->getWpt()->getDescription());
        self::assertSame('Quelle est l\'année de fondation de cet institut ?  1A3A
In welk jaar werd dit instituut ingericht ? 1A3A', $waypoint->getWpt()->getComment());
    }

    /**
     * @covers \LGnap\Reader\DOMXPathReader
     * @covers \LGnap\Reader\GroundspeakReader
     * @covers \LGnap\Reader\GsakWaypointReader
     * @covers \LGnap\Reader\WaypointReader
     * @covers \LGnap\Reader\GpxReader
     * @covers \LGnap\Model\Gpx\GpxWptNode
     * @covers \LGnap\Model\Gpx\GpxGsak
     * @covers \LGnap\Model\Gpx\GpxWpt
     * @covers \LGnap\Model\Gpx\GpxGroundspeak
     */
    public function testExtractUserCommentFromGsak()
    {
        $gpxReader = new GpxReader(__DIR__ . '/../gpx-files/Falcoceline.gpx');

        $waypoints = $gpxReader->extractWaypoints();

        self::assertIsArray($waypoints);
        self::assertCount(119, $waypoints);
        self::assertArrayHasKey('GC7EKAR', $waypoints);

        $waypoint = $waypoints['GC7EKAR'];
        self::assertInstanceOf(GpxWptNode::class, $waypoint);
        self::assertInstanceOf(GpxWpt::class, $waypoint->getWpt());
        self::assertInstanceOf(GpxGsak::class, $waypoint->getGsak());
        self::assertInstanceOf(GpxGroundspeak::class, $waypoint->getGroundspeak());

        self::assertSame('GC7EKAR', $waypoint->getWpt()->getName());
        self::assertSame('Sur un petit sentier froyennois by patvgl (1,5/2)', $waypoint->getWpt()->getDescription());
        self::assertSame('Sur un petit sentier....', $waypoint->getGroundspeak()->getShortDescription());
        self::assertSame('Sur un petit sentier froyennois', $waypoint->getGroundspeak()->getName());
        self::assertSame('La cache peut être atteinte au départ de la rue Louis Masquilier (le plus près) ou de la Chaussée de Lannoy, en passant par le rieu de l\'Welle! Vous vous trouvez aussi à proximité d\'une autre cache, proposée par de très sympathiques riverains-géocacheurs (Louis Masquilier - GC7755Y)! Bonnes découvertes!', $waypoint->getGroundspeak()->getLongDescription());
        self::assertSame('', $waypoint->getWpt()->getComment());
        self::assertSame('Geocache|Traditional Cache', $waypoint->getWpt()->getType());
        self::assertSame('Traditional Cache', $waypoint->getGroundspeak()->getType());

        self::assertSame('', $waypoint->getGsak()->getGcNote());

        $gcCodes = array_keys($waypoints);

        $caches = array_filter($gcCodes, function (string $gcCode) {
            return strpos($gcCode, '7EKAR') !== false;
        });

        self::assertIsArray($caches);
        self::assertCount(1, $caches);

        self::assertArrayHasKey('GC95X0Y', $waypoints);
        $waypoint = $waypoints['GC95X0Y'];
        self::assertInstanceOf(GpxWptNode::class, $waypoint);
        self::assertInstanceOf(GpxWpt::class, $waypoint->getWpt());
        self::assertInstanceOf(GpxGsak::class, $waypoint->getGsak());
        self::assertInstanceOf(GpxGroundspeak::class, $waypoint->getGroundspeak());

        self::assertSame('GC95X0Y', $waypoint->getWpt()->getName());
        self::assertSame('[39] L\'écureuil agile du Tournaisis.', $waypoint->getGroundspeak()->getName());
        self::assertSame('Unknown Cache', $waypoint->getGroundspeak()->getType());
        self::assertSame('Geocache|Unknown Cache', $waypoint->getWpt()->getType());

        self::assertSame('N 50 32.268 E 003 28.089 - CASSENOISETTE - Bonne réponse', $waypoint->getGsak()->getGcNote());

        $caches = array_filter($gcCodes, function (string $gcCode) {
            return strpos($gcCode, '95X0Y') !== false;
        });

        self::assertIsArray($caches);
        self::assertCount(2, $caches);
    }

    /**
     * @covers \LGnap\Reader\DOMXPathReader
     * @covers \LGnap\Reader\GroundspeakReader
     * @covers \LGnap\Reader\GsakWaypointReader
     * @covers \LGnap\Reader\WaypointReader
     * @covers \LGnap\Reader\GpxReader
     * @covers \LGnap\Model\Gpx\GpxWptNode
     * @covers \LGnap\Model\Gpx\GpxWpt
     * @covers \LGnap\Model\Gpx\GpxGsak
     * @covers \LGnap\Model\Gpx\GpxGroundspeak
     * @group ig
     */
    public function testIntegrateChecker()
    {
        foreach (['GC8Y3A9', 'GC9441X', 'GC7X53V'] as $gccode) {
            $gpxReader = new GpxReader(__DIR__ . "/../gpx-files/{$gccode}.gpx");
            $waypoints = $gpxReader->extractWaypoints();

            self::assertIsArray($waypoints);
            self::assertArrayHasKey($gccode, $waypoints);

            $gs = $waypoints[$gccode]->getGroundspeak();
            self::assertNotNull($gs);
            self::assertTrue($gs->hasChecker());
        }

        $gpxReader = new GpxReader(__DIR__ . "/../gpx-files/Bossinet.gpx");
        $waypoints = $gpxReader->extractWaypoints();

        self::assertIsArray($waypoints);

        foreach ($waypoints as $waypoint) {
            $gs = $waypoint->getGroundspeak();
            self::assertNotNull($gs);
            self::assertFalse($gs->hasChecker());
        }
    }
}
