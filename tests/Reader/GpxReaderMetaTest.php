<?php

namespace LGnap\Reader;

use LGnap\Model\Bound;
use PHPUnit\Framework\TestCase;

class GpxReaderMetaTest extends TestCase
{
    /**
     * @covers \LGnap\Reader\GpxReader
     * @covers \LGnap\Model\Bound
     */
    public function testExtractBound()
    {
        $gpxReader = new GpxReader(__DIR__ . '/../gpx-files/MultiAvecWaypoints.gpx');

        $bound = $gpxReader->extractBound();

        self::assertInstanceOf(Bound::class, $bound);
        self::assertSame(50.59555, $bound->getMinLatitude());
        self::assertSame(4.323, $bound->getMinLongitude());
        self::assertSame(50.598967, $bound->getMaxLatitude());
        self::assertSame(4.328367, $bound->getMaxLongitude());
    }

    /**
     * @covers \LGnap\Reader\GpxReader
     * @covers \LGnap\Model\Bound
     */
    public function testExtractBoundNull()
    {
        $gpxReader = new GpxReader(__DIR__ . '/../gpx-files/BM8NJV4.gpx');

        self::assertNull($gpxReader->extractBound());
    }
}
